using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Builder
{
    public class RegisterAnalyzer
    {
        private AssemblyInfo AssemblyInfo { get; set; }

        public RegisterAnalyzer()
        {
            AssemblyInfo = new AssemblyInfo();
        }

        public List<string> ParamsServices(string serviceName)
        {
            var type = AssemblyInfo.GetTypeByName(serviceName);
            if (type.IsInterface)
            {
                type = AppDomain.CurrentDomain
                    .GetAssemblies()
                    .Where(x => !x.IsDynamic)
                    .SelectMany(x => x.GetExportedTypes())
                    .Single(x => x.GetInterfaces().Contains(type));

            }

            var constructor = type.GetConstructors().Min(x => x.GetParameters());
            var parametersName = constructor.Select(x => x.ParameterType.FullName);
            return parametersName.ToList();
        }
    }
}