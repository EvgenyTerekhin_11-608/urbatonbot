using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Builder
{
    public class TypeComparer : IComparer<Type>
    {
        private List<Type> _types;

        public TypeComparer(IEnumerable<Type> types)
        {
            this._types = types.ToList();
        }

        public int Compare(Type x, Type y)
        {
            x = TryReplaceImplementInterface(x);
            y = TryReplaceImplementInterface(y);
            var xPosition = _types.IndexOf(x);
            var yPosition = _types.IndexOf(y);
            if (xPosition > yPosition)
                return 1;
            if (xPosition < yPosition)
                return -1;
            return 0;
        }

        Type TryReplaceImplementInterface(Type x)
        {
            if (x.GetInterfaces().Any(xx=>_types.Contains(xx)) && !_types.Contains(x))
            {
               return x.GetInterfaces().First(xx => _types.Contains(xx));
            }

            return x;
        }
    }
}