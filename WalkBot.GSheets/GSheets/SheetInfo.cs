using System;

namespace FMS.GSheets.GSheets
{
    public class SheetInfo
    {
        [Obsolete("Only implicit calls", true)]
        public SheetInfo()
        {
            
        }

        public SheetInfo(string id, string sheet, string range)
        {
            Id = id;
            Sheet = sheet;
            Range = range;
        }

        public string Id { get; set; }
        public string Sheet { get; set; }
        public string Range { get; set; }

        public string ConstructRange()
        {
            var s="";
            if (Sheet != "") s += Sheet;
            if (Sheet != "" && Range != "") s += "!";
            if (Range != "") s += Range;
            return s;
        }
    }
}