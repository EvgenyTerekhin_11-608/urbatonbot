using System.Collections.Generic;

namespace FMS.GSheets.GSheets.State
{
    public class StateInfo
    {
        public List<string> UseMeCheckers { get; set; }
        public List<string> Commands { get; set; }
        public List<string> Validators { get; set; }

        public List<string> ContainsMessage { get; set; }
        public string FromToState { get; set; }
    }
}