using System;

namespace FMS.Infrastructure.Infrastructure
{
    public class DirectoryCreator
    {
        public string Directory { get; }

        public override string ToString()
        {
            return Directory;
        }
        
        public DirectoryCreator()
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory;
            var n = directory.IndexOf("bin");

            if(n>0)
                directory = directory.Substring(0, n - 1);
            
            /*for (var i = 0; i < directory.Length - 3; i++)
            {
                if (directory[i] == 'b' && directory[i + 1] == 'i' && directory[i + 2] == 'n')
                {
                    directory = directory.Substring(0, i - 1);
                }
            }*/

            Directory = directory;
        }
    }
}