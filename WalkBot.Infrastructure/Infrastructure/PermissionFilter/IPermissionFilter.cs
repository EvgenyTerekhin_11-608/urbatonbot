using System.Collections.Generic;

namespace FMS.Infrastructure.Infrastructure.PermissionFilter
{
    public interface IPermissionFilter<TEntity>
    {
        IEnumerable<TEntity> Filter(IEnumerable<TEntity> infoStorage);
    }
}