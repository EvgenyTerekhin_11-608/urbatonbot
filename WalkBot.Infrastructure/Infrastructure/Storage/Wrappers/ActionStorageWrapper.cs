using System.Collections.Generic;
using FMS.Infrastructure.Extensions;
using FMS.Infrastructure.Infrastructure.UserService;

namespace FMS.Infrastructure.Infrastructure.Storage.Wrappers
{
    public class ActionStorageWrapper : StorageWrapper<UserActionInfoStorage>, IMutableValueWrapper
    {
        public ActionStorageWrapper(UserActionInfoStorage storage, IUserService userService) : base(storage,
            userService)
        {
        }

        public T Set<T>() where T : class
        {
            return Storage.Set<UserActionInfoStorage, T>();
        }

        public T Set<T>(string name)
            where T : class
        {
            return Storage.Set<UserActionInfoStorage, T>(name);
        }

        public List<T> SetCollection<T>() where T : class
        {
            return Storage.Set<UserActionInfoStorage, List<T>>();
        }
    }
}