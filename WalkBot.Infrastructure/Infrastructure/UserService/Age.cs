using System.ComponentModel.DataAnnotations;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public enum Age
    {
        [Display(Name = "До 18")]
        Before18,

        [Display(Name = "18-26")]
        Eigtheen_TwentySix,

        [Display(Name = "26-35")]
        TwentySix_ThirtyFive,

        [Display(Name = "Больше 35")]
        AfterThirtyFive
    }
}