using System.ComponentModel.DataAnnotations;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public enum Cost
    {
        [Display(Name = "Бесплатно")]
        Free,
        [Display(Name = "Низкий")]
        Low,
        [Display(Name = "Средний")]
        Medium,
        [Display(Name = "Высокий")]
        High,
        [Display(Name = "Неограниченный")]
        UnLimited,
    }
}