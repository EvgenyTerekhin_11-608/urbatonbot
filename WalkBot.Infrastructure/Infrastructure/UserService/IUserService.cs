namespace FMS.Infrastructure.Infrastructure.UserService
{
    public interface IUserService
    {
        string Username();
        long ChatId();
    }
}