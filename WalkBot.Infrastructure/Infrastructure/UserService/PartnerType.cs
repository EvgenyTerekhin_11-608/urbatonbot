using System.ComponentModel.DataAnnotations;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public enum PartnerType
    {
        [Display(Name = "С парнем/девушкой")]
        Girl,
        [Display(Name = "С друзьями")]
        Friends,
        [Display(Name = "В одиночку")]
        Single,
        [Display(Name = "С детьми")]
        WithChildren
    }
}