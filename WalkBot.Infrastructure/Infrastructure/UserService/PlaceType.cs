using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public static class PlaceTypeExtensions
    {
        public static IEnumerable<string> GetFilter(this PlaceType placeType)
        {
            return typeof(PlaceType).GetMember(placeType.ToString()).First().GetCustomAttribute<ConvertToAttribute>()
                .GetFilterType();
        }
    }

    public class ConvertToAttribute : Attribute
    {
        readonly FilterType _ft;

        public ConvertToAttribute(FilterType ft)
        {
            _ft = ft;
        }

        public IEnumerable<string> GetFilterType()
        {
            var strings = _ft.ToString().Split(", ");
            var memberInfos = typeof(FilterType).GetMembers().Where(x => strings.Contains(x.Name)).ToList();
            return memberInfos.Select(x => x.GetCustomAttribute<DisplayAttribute>().Name).ToList();
        }
    }

    [Flags]
    public enum FilterType
    {
        [Display(Name = "is_amusement_park")]
        AmusementType = 1,

        [Display(Name = "is_aquarium")]
        Aquarium = 2,

        [Display(Name = "is_art_gallery")]
        ArtGallery = 4,

        [Display(Name = "is_bowling_alley")]
        BowlingAlley = 8,

        [Display(Name = "is_library")]
        Library = 16,

        [Display(Name = "is_movie_theater")]
        MovieTheater = 32,

        [Display(Name = "is_mosque")]
        Mosque = 64,

        [Display(Name = "is_cafe")]
        Cafe = 128,

        [Display(Name = "is_church")]
        Church = 256,

        [Display(Name = "is_night_club")]
        NightClub = 512,

        [Display(Name = "is_park")]
        Park = 1024,

        [Display(Name = "is_museum")]
        Museum = 2048,

        [Display(Name = "is_restaurant")]
        Restaraunt = 4096,

        [Display(Name = "is_zoo")]
        Zoo = 8192
    }

    public enum PlaceType
    {
        [Display(Name = "Улица")]
        Street,

        [ConvertTo(FilterType.Cafe | FilterType.Restaraunt)]
        [Display(Name = "Перекус")]
        Snack,

        [ConvertTo(FilterType.Church | FilterType.Aquarium | FilterType.Library | FilterType.Mosque | FilterType.Park |
                   FilterType.Zoo | FilterType.ArtGallery | FilterType.MovieTheater)]
        [Display(Name = "Культурный отдых")]
        Culture,

        [ConvertTo(FilterType.BowlingAlley)]
        [Display(Name = "Спортивный отдых")]
        Sport,

        [ConvertTo(FilterType.AmusementType | FilterType.NightClub)]
        [Display(Name = "Развлечения")]
        Entertainment
    }
}