using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Google.Apis.Util;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.Extensions
{
    public static class CommandExtensions
    {
        public static InlineKeyboardMarkup AddBack(this Command command, List<InlineKeyboardButton> buttons)
        {
            int teil;
            if (buttons.Count % 2 == 0)
                teil = 2;
            else
                teil = 3;
            buttons.Add("Назад");

            return new InlineKeyboardMarkup(buttons.ToTeil(teil));
        }

        public static ReplyKeyboardMarkup AddBackReply(this Command command, List<KeyboardButton> buttons)
        {
            buttons.Add("Назад");

            return new ReplyKeyboardMarkup(buttons.ToTeil(1), true, true);
        }

        public static IEnumerable<InlineKeyboardButton> EnumFilter<T>(
            this IEnumerable<InlineKeyboardButton> list, IEnumerable<T> hidden)
        {
            return list.Where(x => !hidden.Contains((T) (object) int.Parse(x.CallbackData)));
        }

        public static IEnumerable<string> EnumDisplayNames(this Type enumType)
        {
            var buttons1 = enumType.GetMembers()
                .Select(x => x.GetCustomAttribute<DisplayAttribute>())
                .Where(x => x != null)
                .Select(x => x.Name).ToList();

            return buttons1;
        }

        public static IEnumerable<IEnumerable<InlineKeyboardButton>> Format<EnumType>(this IEnumerable<EnumType> hidden)
        {
            return typeof(EnumType).GetMembers()
                .Select(x => x.GetCustomAttribute<DisplayAttribute>())
                .Where(x => x != null)
                .Select(x => x.Name)
                .Select((x, i) => new InlineKeyboardButton {Text = x, CallbackData = i.ToString()})
                .EnumFilter(hidden)
                .ToTeil(2);
        }
    }
}