﻿using System;
using System.Linq;

namespace WalkBot.TelegramBot.Extensions
{
    public static class Reflect
    {
        public static object Move(object value, params string[] properties)
        {
            var currentValue = value;
            foreach (var property in properties)
            {
                if (currentValue == null)
                    return null;
                var prop = currentValue.GetType().GetProperties().FirstOrDefault(x => x.Name == property) ??
                           throw new Exception("Такого свойства не существует");
                currentValue = prop.GetValue(currentValue);
            }


            return currentValue;
        }
    }
}