using System.Collections.Generic;

namespace WalkBot.TelegramBot.Extensions
{
    public static class StringExtensions
    {
        private static Dictionary<char, string> _dict = new Dictionary<char,string>()
        {
            {'q', "й"},
            {'w', "ц"},
            {'e', "у"},
            {'r', "к"},
            {'t', "е"},
            {'y', "н"},
            {'u', "г"},
            {'i', "ш"},
            {'o', "щ"},
            {'p', "з"},
            {'[', "х"},
            {']', "ъ"},
            {'a', "ф"},
            {'s', "ы"},
            {'d', "в"},
            {'f', "а"},
            {'g', "п"},
            {'h', "р"},
            {'j', "о"},
            {'k', "л"},
            {'l', "д"},
            {';', "ж"},
            {'\'', "э"},
            {'z', "я"},
            {'x', "ч"},
            {'c', "с"},
            {'v', "м"},
            {'b', "и"},
            {'n', "т"},
            {'m', "ь"},
            {',', "б"},
            {'/', "."},
            {' ', " "},
            {'`', "ё"},
            {'\\', "\\"},
            {'@', "\""},
            {'#', "№"},
            {'$', ";"},
            {'^', ":"},
            {'&', "?"},
            {'|', "/"},
            {'Q', "Й"},
            {'W', "Ц"},
            {'E', "У"},
            {'R', "К"},
            {'T', "Е"},
            {'Y', "Н"},
            {'U', "Г"},
            {'I', "Ш"},
            {'O', "Щ"},
            {'P', "З"},
            {'{', "Х"},
            {'}', "Ъ"},
            {'A', "Ф"},
            {'S', "Ы"},
            {'D', "В"},
            {'F', "А"},
            {'G', "П"},
            {'H', "Р"},
            {'J', "О"},
            {'K', "Л"},
            {'L', "Д"},
            {':', "Ж"},
            {'\"', "Э"},
            {'Z', "Я"},
            {'X', "Ч"},
            {'C', "С"},
            {'V', "М"},
            {'B', "И"},
            {'N', "Т"},
            {'M', "Ь"},
            {'<', "Б"},
            {'>', "Ю"},      
        };
        public static string Autoconvert(this string text)
        {
            string s="";
            
            for (int i = 0; i < text.Length; i++)
            {
                if (_dict.ContainsKey(text[i]))
                    s += _dict[text[i]];
                else
                    s += text[i];
            }
            
            return s;
        }

        public static string FixEmpName(this string name)
        {
            if (name.Length > 33)
            {
                name = name.Substring(0, 33);
            }

            return name;
        }
    }
}