﻿using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.Extensions
{
    public static class TelegramUserExtentions
    {
        public static object WrapGetId(this Update update) => Reflect.Move(update, "Message", "From", "Id");
        public static object WrapUserMessageText(this Update update) => Reflect.Move(update, "Message", "Text");

        public static bool Contains(this Message message, string str)
        {
            return
                message?.Text?.Contains(str) ?? false;
        }
    }

    public static class ListExtentions
    {
        public static IEnumerable<IEnumerable<T>> ToTeil<T>(this System.Collections.Generic.IEnumerable<T> enumerable,
            int tail)
        {
            var s = enumerable.Select((x, i) => enumerable.Skip(tail * i).Take(tail));
            return s;
        }
    }
}