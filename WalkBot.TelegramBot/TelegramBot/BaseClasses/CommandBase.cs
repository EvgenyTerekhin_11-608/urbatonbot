using System.Collections.Generic;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.TelegramBot.Validators;
using WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators;

namespace WalkBot.TelegramBot.TelegramBot.BaseClasses
{
    public class CommandBase
    {
        public readonly List<PredicateValidator> Validators;
        public readonly List<ContainsValidator> ContainsValidator;
        private readonly TransactionManager _transactionManager;
        public List<UseMe> UseMeCommands { get; set; }

        public List<Command> Commands { get; set; }

        public CommandBase(
            List<UseMe> useMeCommands,
            List<Command> commands,
            List<PredicateValidator> validators,
            List<ContainsValidator> containsValidator,
            TransactionManager transactionManager)
        {
            _transactionManager = transactionManager;
            UseMeCommands = useMeCommands;
            Commands = commands;
            ContainsValidator = containsValidator;
            Validators = validators;
        }

        public void ChangeState()
        {
            _transactionManager.ChangeState();
        }

        public void ExecuteCommands(Update update)
        {
            Commands.ForEach(
                x => 
                    x.Execute(update));
        }
    }
}