using FMS.GSheets.GSheets.State;
using FMS.Infrastructure.Infrastructure.Storage.Wrappers;
using FMS.Infrastructure.Infrastructure.UserService;

namespace WalkBot.TelegramBot.TelegramBot.BaseClasses
{
    public class TransactionManager
    {
        private readonly StateType _stateType;
        private readonly ActionStorageWrapper _wrapper;

        public TransactionManager(
            StateType stateType,
            ActionStorageWrapper wrapper)
        {
            _stateType = stateType;
            _wrapper = wrapper;
        }

        public void ChangeState()
        {
            _wrapper.Set<CurrentStateInfo>().ChangeState(_stateType);
        }
    }
}