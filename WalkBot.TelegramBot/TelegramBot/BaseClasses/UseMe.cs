using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.TelegramBot.BaseClasses
{
    public abstract class UseMe
    {
        private readonly UrbatonDbContext.UrbatonDbContext _context;

        protected UseMe(UrbatonDbContext.UrbatonDbContext context)
        {
            _context = context;
        }

        public abstract bool Check(Update update);
    }
}