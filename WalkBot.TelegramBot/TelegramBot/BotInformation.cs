﻿using System;

namespace WalkBot.TelegramBot.TelegramBot
{
    public class BotInformation
    {
        [Obsolete("Only implicit calls", true)]
        public BotInformation()
        {
            
        }
        
        public BotInformation(string token, string proxy, string username, string password)
        {
            Token = token;
            Proxy = proxy;
            Username = username;
            Password = password;
        }

        public string Token { get; set; }
        public string Proxy { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}