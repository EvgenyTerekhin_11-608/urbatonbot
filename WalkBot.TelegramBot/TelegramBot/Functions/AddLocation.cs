using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class AddLocation: Command
    {
        public AddLocation(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if (user != null)
            {
                user.CurrentState.Latitude = update.Message.Location.Latitude;
                user.CurrentState.Longitude = update.Message.Location.Longitude;
            }
        }
    }
}