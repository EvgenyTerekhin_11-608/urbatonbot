using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChooseAge : Command
    {
        public ChooseAge(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var hidden =
                EnumFilter.GetHidden<PartnerType, Age>(_context.GetUser(update.GetChatId()).CurrentState.PartnerType);

            var keyboard = hidden.Format();
            if (update.CallbackQuery != null)
                Sender.SendEdit("Выберите возраст", update.CallbackQuery.Message.MessageId,
                    new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите возраст", new InlineKeyboardMarkup(keyboard));
        }
    }
}