using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChooseCost : Command
    {
        public ChooseCost(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var state = _context.GetUser(update.GetChatId()).CurrentState;
            var hidden =
                EnumFilter.GetHidden<PartnerType, Age, Cost>(state.PartnerType, state.Age);
            var keyboard = hidden.Format();
            if (update.CallbackQuery != null)
                Sender.SendEdit("Выберите бюджет прогулки", update.CallbackQuery.Message.MessageId,
                    new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите бюджет прогулки", new InlineKeyboardMarkup(keyboard));
        }
    }
}