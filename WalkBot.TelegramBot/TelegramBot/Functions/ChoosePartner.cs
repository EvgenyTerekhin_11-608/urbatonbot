using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChoosePartner : Command
    {
        public override void Execute(Update update)
        {

            var buttons = new List<string>
            {
                "С парнем/девушкой",
                "С друзьями",
                "В одиночку",
                "С детьми"
            };

            var keyboard = buttons.Select((x, i) => new InlineKeyboardButton {Text = x, CallbackData = i.ToString()})
                .ToTeil(2);
            if(update.CallbackQuery!=null)
                Sender.SendEdit("Выберите, с кем пойдете", update.CallbackQuery.Message.MessageId, new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите, с кем пойдете", new InlineKeyboardMarkup(keyboard));
        }

        public ChoosePartner(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }
    }
}