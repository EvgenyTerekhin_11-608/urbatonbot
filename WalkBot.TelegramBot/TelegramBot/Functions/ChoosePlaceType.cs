using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChoosePlaceType : Command
    {
        public ChoosePlaceType(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var state = _context.GetUser(update.GetChatId()).CurrentState;
            var hidden = EnumFilter.GetHidden<PartnerType, Age, Cost, RelaxationType, PlaceType>(
                state.PartnerType, state.Age, state.Cost, state.RelaxationType);

            var keyboard = hidden.Format();
            if (update.CallbackQuery != null)
                Sender.SendEdit("Выберите место", update.CallbackQuery.Message.MessageId,
                    new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите место", new InlineKeyboardMarkup(keyboard));
        }
    }
}