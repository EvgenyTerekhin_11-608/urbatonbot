using System.Collections.Generic;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class SendLocation : Command
    {
        public SendLocation(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            Sender.SendLocation((float) 55.7888503, (float) 49.1142896);
            Sender.Send("Хотите ли вы продолжить прогулку?", new ReplyKeyboardMarkup(new List<KeyboardButton>
            {
                new KeyboardButton("Продолжить"){RequestLocation = true},
                new KeyboardButton("Закончить")
            }.ToTeil(2)){ResizeKeyboard = true});
        }
    }
}