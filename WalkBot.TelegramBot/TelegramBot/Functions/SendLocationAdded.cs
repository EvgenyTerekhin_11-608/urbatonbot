using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class SendLocationAdded: Command
    {
        public SendLocationAdded(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            Sender.Send("Местоположение определено", new ReplyKeyboardRemove());
        }
    }
}