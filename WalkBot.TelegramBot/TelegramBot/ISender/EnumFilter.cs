using System;
using System.Collections.Generic;
using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;

namespace WalkBot.TelegramBot.TelegramBot.ISender
{
    public interface IEnumFilter<TPredicateEnum, TResultEnum>
    {
        IEnumerable<TResultEnum> GetHidden(TPredicateEnum predicateEnum);
    }

    public class EnumFilter
    {
        public Dictionary<RelaxationType, PlaceType[]> relaxationTypeHiddenDictionary =>
            new Dictionary<RelaxationType, PlaceType[]>()
            {
                {
                    RelaxationType.Passive, new[] {PlaceType.Sport, PlaceType.Entertainment}
                }
            };

        private IEnumerable<(Type, Type, object)> dictionaries => GetType().GetProperties()
            .Select(x => x)
            .Where(x => x.PropertyType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            .Select(x => (x.PropertyType.GenericTypeArguments[0], x.PropertyType.GenericTypeArguments[1],
                x.GetValue(this)))
            .ToList();

        public IEnumerable<TResultEnum> GetHidden<TPredicateEnum, TResultEnum>(TPredicateEnum predicateEnum)
        {
            if (predicateEnum == null)
                throw new ArgumentException("value is null");
            var hidden =
                dictionaries.FirstOrDefault(x => x.Item1 == typeof(TPredicateEnum) && x.Item2 == typeof(TResultEnum[]));
            if (hidden == default)
                return new List<TResultEnum>();
            var d = (Dictionary<TPredicateEnum, TResultEnum[]>) hidden.Item3;
            if (d.TryGetValue(predicateEnum, out var value))
                return value;
            return new List<TResultEnum>();
        }

        public IEnumerable<TResultEnum> GetHidden<TPredicateEnum, TPredicateEnum1, TResultEnum>(
            TPredicateEnum predicateEnum, TPredicateEnum1 predicateEnum1)
        {
            var one = GetHidden<TPredicateEnum, TResultEnum>(predicateEnum);
            var two = GetHidden<TPredicateEnum1, TResultEnum>(predicateEnum1);
            return one.Concat(two).Distinct();
        }

        public IEnumerable<TResultEnum> GetHidden<TPredicateEnum, TPredicateEnum1, TPredicateEnum2, TResultEnum>(
            TPredicateEnum predicateEnum, TPredicateEnum1 predicateEnum1, TPredicateEnum2 predicateEnum2)
        {
            var one = GetHidden<TPredicateEnum, TPredicateEnum1, TResultEnum>(predicateEnum, predicateEnum1);
            var two = GetHidden<TPredicateEnum2, TResultEnum>(predicateEnum2);
            return one.Concat(two).Distinct();
        }

        public IEnumerable<TResultEnum> GetHidden<TPredicateEnum, TPredicateEnum1, TPredicateEnum2, TPredicateEnum3,
            TResultEnum>(
            TPredicateEnum predicateEnum, TPredicateEnum1 predicateEnum1, TPredicateEnum2 predicateEnum2,
            TPredicateEnum3 predicateEnum3)
        {
            var one = GetHidden<TPredicateEnum, TPredicateEnum1, TPredicateEnum3, TResultEnum>(predicateEnum,
                predicateEnum1, predicateEnum3);
            var two = GetHidden<TPredicateEnum2, TResultEnum>(predicateEnum2);
            return one.Concat(two).Distinct();
        }


        public IEnumerable<TResultEnum> GetHidden<TPredicateEnum, TPredicateEnum1, TPredicateEnum2, TPredicateEnum3,
            TPredicateEnum4, TResultEnum>(
            TPredicateEnum predicateEnum, TPredicateEnum1 predicateEnum1, TPredicateEnum2 predicateEnum2,
            TPredicateEnum3 predicateEnum3, TPredicateEnum4 predicateEnum4)
        {
            var one = GetHidden<TPredicateEnum, TPredicateEnum1, TPredicateEnum3, TPredicateEnum4, TResultEnum>(
                predicateEnum,
                predicateEnum1, predicateEnum3, predicateEnum4);
            var two = GetHidden<TPredicateEnum2, TResultEnum>(predicateEnum2);
            return one.Concat(two).Distinct();
        }
    }
}