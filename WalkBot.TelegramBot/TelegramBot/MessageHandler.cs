﻿using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.TelegramBot
{
    public class MessageHandler
    {
        private readonly State.State _state;
        private readonly Update _update;

        public MessageHandler(State.State state, Update update)
        {
            _state = state;
            _update = update;
        }

        public void Handle()
        {
            _state.Execute(_update);
        }
    }
}