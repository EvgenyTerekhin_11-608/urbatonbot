namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class ButtonValidator : PredicateValidator
    {
        public ButtonValidator() : base(x => x?.CallbackQuery?.Data != null)
        {
        }
    }
}