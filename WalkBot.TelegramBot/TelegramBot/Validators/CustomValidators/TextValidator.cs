namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class TextValidator : PredicateValidator
    {
        public TextValidator() : base(x => x?.Message?.Text != null)
        {
        }
    }
}